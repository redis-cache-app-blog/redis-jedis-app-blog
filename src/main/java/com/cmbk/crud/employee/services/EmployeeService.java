package com.cmbk.crud.employee.services;

import com.cmbk.crud.employee.domain.CreateEmployeeRequest;
import com.cmbk.crud.employee.domain.CreateEmployeeResponse;
import com.cmbk.crud.employee.domain.EmployeeResponse;

/**
 * @author chanaka.k
 *
 */
public interface EmployeeService {

	public CreateEmployeeResponse createEmployee(CreateEmployeeRequest createEmployeeRequest);

	public EmployeeResponse retrieveEmployeeById(String employeeID);

}
