package com.cmbk.crud.employee.services.cache;

import com.cmbk.crud.employee.domain.Employee;

/**
 * @author chanaka.k
 *
 */
public interface RedisCacheService {

	/**
	 * Store Employee object
	 * 
	 * @param employeeId as a key
	 * @param employee   as a value
	 * @return Employee
	 */
	Employee storeEmployee(String employeeId, Employee employee);

	/**
	 * Retrieve Employee object
	 * 
	 * @param employeeId as a key
	 * @return Employee
	 */
	Employee retrieveEmployee(String employeeId);

	/**
	 * Flush employee object using given employeeId
	 * 
	 * @param employeeId as a key
	 */
	void flushEmployeeCache(String employeeId);

	/**
	 * Flush all data
	 * 
	 */
	void clearAll();

}
