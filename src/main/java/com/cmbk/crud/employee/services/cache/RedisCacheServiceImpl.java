package com.cmbk.crud.employee.services.cache;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.cmbk.crud.employee.domain.Employee;
import com.google.gson.Gson;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

/**
 * @author chanaka.k
 *
 */
@Service
public class RedisCacheServiceImpl implements RedisCacheService {

	@Autowired
	private JedisPool jedisPool;

	private final Gson gson = new Gson();

	//TTL(Time to live) of session data in seconds 
	@Value("${redis.sessiondata.ttl}")
	private int sessiondataTTL;

	private final Logger logger = LogManager.getLogger(RedisCacheServiceImpl.class);

	// Acquire Jedis instance from the jedis pool.
	private Jedis acquireJedisInstance() {

		return jedisPool.getResource();
	}

	// Releasing the current Jedis instance once completed the job.
	private void releaseJedisInstance(Jedis jedis) {

		if (jedis != null) {
			jedis.close();
			jedis = null;
		}
	}

	@Override
	public Employee storeEmployee(String employeeId, Employee employee) {

		Jedis jedis = null;

		try {

			jedis = acquireJedisInstance();

			String json = gson.toJson(employee);
			jedis.set(employeeId, json);
			jedis.expire(employeeId, sessiondataTTL);

		} catch (Exception e) {
			logger.error("Error occured while storing data to the cache ", e.getMessage());
			releaseJedisInstance(jedis);
			throw new RuntimeException(e);

		} finally {
			releaseJedisInstance(jedis);
		}

		return employee;
	}

	@Override
	public Employee retrieveEmployee(String employeeId) {

		Jedis jedis = null;

		try {

			jedis = acquireJedisInstance();

			String employeeJson = jedis.get(employeeId);

			if (StringUtils.hasText(employeeJson)) {
				return gson.fromJson(employeeJson, Employee.class);
			}

		} catch (Exception e) {
			logger.error("Error occured while retrieving data from the cache ", e.getMessage());
			releaseJedisInstance(jedis);
			throw new RuntimeException(e);

		} finally {
			releaseJedisInstance(jedis);
		}

		return null;
	}

	@Override
	public void flushEmployeeCache(String employeeId) {

		Jedis jedis = null;
		try {

			jedis = acquireJedisInstance();

			List<String> keys = jedis.lrange(employeeId, 0, -1);
			if (!CollectionUtils.isEmpty(keys)) {
				// add the list key in as well
				keys.add(employeeId);

				// delete the keys and list
				jedis.del(keys.toArray(new String[keys.size()]));
			}
		} catch (Exception e) {
			logger.error("Error occured while flushing specific data from the cache ", e.getMessage());
			releaseJedisInstance(jedis);
			throw new RuntimeException(e);

		} finally {
			releaseJedisInstance(jedis);
		}

	}

	@Override
	public void clearAll() {

		Jedis jedis = null;
		try {

			jedis = acquireJedisInstance();
			jedis.flushAll();

		} catch (Exception e) {
			logger.error("Error occured while flushing all data from the cache ", e.getMessage());
			releaseJedisInstance(jedis);
			throw new RuntimeException(e);

		} finally {
			releaseJedisInstance(jedis);
		}

	}

}
