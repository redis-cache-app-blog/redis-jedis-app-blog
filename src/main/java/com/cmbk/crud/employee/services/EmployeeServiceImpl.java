package com.cmbk.crud.employee.services;

import java.util.Optional;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpServerErrorException;

import com.cmbk.crud.employee.domain.CreateEmployeeRequest;
import com.cmbk.crud.employee.domain.CreateEmployeeResponse;
import com.cmbk.crud.employee.domain.Employee;
import com.cmbk.crud.employee.domain.EmployeeResponse;
import com.cmbk.crud.employee.domain.common.exception.NotFoundException;
import com.cmbk.crud.employee.domain.transform.EmployeeTransformer;
import com.cmbk.crud.employee.repositories.EmployeeRepository;
import com.cmbk.crud.employee.services.cache.RedisCacheService;

/**
 * @author chanaka.k
 *
 */
@Service
public class EmployeeServiceImpl implements EmployeeService {

	final static Logger logger = Logger.getLogger(EmployeeServiceImpl.class);

	@Autowired
	private EmployeeRepository employeeRepository;
	
	@Autowired
	private RedisCacheService redisCacheService;

	@Override
	public CreateEmployeeResponse createEmployee(CreateEmployeeRequest createEmployeeRequest) {

		logger.info("Inside create employee service");

		Employee employee = null;
		CreateEmployeeResponse response = null;

		try {

			employee = EmployeeTransformer.transformEmployeeRequestToDto(createEmployeeRequest);
			employee = employeeRepository.save(employee);
			
			// Store the Employee object in the cache. Employee id is a key and employee object as a value.
			redisCacheService.storeEmployee(employee.getId(), employee);
			
			response = EmployeeTransformer.transformEmployeeDtoToResponse(employee);

		} catch (HttpServerErrorException hse) {
			logger.error("Server Error found : " + hse.getMessage().toString());
			hse.printStackTrace();
		} catch (Exception e) {
			logger.error("Error found : " + e.getMessage().toString());
			e.printStackTrace();
		}

		logger.info("Successfully saved new employee");

		return response;
	}

	@Override
	public EmployeeResponse retrieveEmployeeById(String employeeId) {

		logger.info("Inside retrieve employee by id, service");

		Optional<Employee> optionalEmployee = null;
		Employee employee = null;
		EmployeeResponse employeeResponse = null;
		
		// First trying to retrieve the employee object using employeeId from the cache.
		employee = redisCacheService.retrieveEmployee(employeeId);
		
		//If the employee object not available in the cache DB, then need to retrieve it from the Mongo DB.
		if(employee == null) {
			
			optionalEmployee = employeeRepository.findById(employeeId);

			if (optionalEmployee.isPresent()) {
				employee = optionalEmployee.get();
			} else {
				throw new NotFoundException("INVALID_EMPLOYEE_ID", "Employee not found for the given id");
			}
		}

		employeeResponse = EmployeeTransformer.employeeToEmployeeResponse(employee);

		logger.info("Successfully returned employee by id");
		return employeeResponse;
	}

}
