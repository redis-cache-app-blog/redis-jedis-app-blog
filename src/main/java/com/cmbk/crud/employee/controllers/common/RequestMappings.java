package com.cmbk.crud.employee.controllers.common;

/**
 * @author chanaka.k
 *
 */
public interface RequestMappings {
	
	public static String EMPLOYEES = "employees";
		
	public static String CREATE_EMPLOYEE = "/add";
	
	public static String RETRIVE_EMPLOYEE_BY_ID = "/{employee-id}";
	
	
}
